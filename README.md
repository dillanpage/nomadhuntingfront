# NomadHunting
¿Por qué Nomad Hunting?


## 1. Motivacion

Como me gusta viajar, buscaba plataformas donde la poder compartir tus viajes, ver qué visita la gente y descubrir nuevos lugares. Mi gran decepción fue no encontrar ninguna aplicación / web con la que me sintiera cómodo. La motivación de este proyecto es que, así como no me he sentido cómodo con ninguna aplicación relacionada, poder ofrecer una herramienta para organizar mejor los viajes de un modo social.

## 2. Objetivos

Ofrecer a los usuarios que les gusta viajar, una plataforma donde compartir sus viajes, conocer gente con gustos similares, ver los lugares que generan más interés y añadirlos a una lista de deseos para organizar un viaje de la manera más cómoda posible

El objetivo es que los usuarios se sientan cómodos con una plataforma fácil de usar, donde poder almacenar tus recuerdos de viajes y conocer nuevos lugares o puntos de interés turístico.


## 3. Demostración del funcionamiento de la aplicación

Como usuario regular:

- Ver lugares y puntos de interés, pudiendo ser filtrados por categorías.

Como usuario registrado: 

- Poder dar like a los puntos de interés turísticos.
- Añadir viajes con los puntos de interés visitados.
- Añadir reseñas con imágenes a los puntos de interés visitados.
- Sugerir que se añada información a la plataforma.
- Buscar el perfil de otros usuarios para ver sus viajes.

Como administrador: 

- Gestionar reseñas de usuarios (aprobar / rechazar).
- Obtener y almacenar datos en la plataforma (de API externo).
- Ver los lugares más deseados de los usuarios.
- Panel crud para gestionar lugares y puntos de interés.


## 4. TDD

## 5. Descripción técnica

Arquitectura Aplicación.

Se ha utilizado el modelo de tres capas.

![arquitecture](https://toniramon.dev/arquitectura_app.png)

Arquitectura AWS

![arquitecture](https://toniramon.dev/NomadHunting.png)

Diagrama de componentes

Front End

![front](https://toniramon.dev/umlfront.png)

Back End

![back](https://toniramon.dev/umlback.png)

Diagrama E/R

![db completo](https://toniramon.dev/nh_database_complete.png)


## 6. Metodologías de Desarrollo.

El flujo de trabajo en Git ha sido seguir el estándar de Git Flow, es decir, tenemos la rama Máster que es donde se desplegará automáticamente a producción con unos pipeline, Beta que no la he utilizado al ser una app pequeña, Develop donde desarrollamos nuevas features y desde esta rama, para cada feature / bug / etc, creamos una rama secundária. Desde esta rama añadimos la funcionalidad y lo testeamos, al estar todo correcto se mergea en Develop y cuando es necesario sube hasta Máser.

![Git Flow](https://i.stack.imgur.com/RSAAo.png)

## 7. Diagrama de GANTT

Diagrama de Gantt: antes del desarrollo.

![Gantt prev](https://toniramon.dev/gantt-previo.png)

Diagrama de Gantt completo: 
![Gantt post](https://toniramon.dev/gantt_final_hu1.png)
![Gantt post](https://toniramon.dev/gantt_final_hu2.png)

He tardado algo más de lo esperado porque hice varios refactor estéticos de la vista Principal y la vista Place. 

Además, al utilizar un API externo para scarpar los datos con un límite de peticiones me retrasó la generación de contenido.


## 8. Tiempo invertido en el desarrollo

Tiempo Estimado: 110 horas.

![arquitecture](https://toniramon.dev/tiempo_inicial.png)

Tiempo final 133 horas.
![time spent](https://toniramon.dev/time_spent_1.png)
![time spent](https://toniramon.dev/time_spent_2.png)


## 9. Presupuesto de la App para el comprador, coste del software.

![time spent](https://toniramon.dev/presupuesto.png)

## 10. Conclusiones.

Posibles mejoras: Muchas!!
- Autologin tras registrarse.

- Utilizar repositorios en backend y desacoplar lógica de controladores.

- Validar más estrictamente las peticiones que recibe el API Rest.

- Mejorar el ORM de Laravel para que las relaciones entre tablas hagan el correcto uso del update y delete de las Foreign Key.

Principales dificultades:

- El uso de Vue para realizar el Front ha sido un reto, al no tener experiéncia en el propio framework y muy poca con el lenguaje Javascript.
- El uso de TailwindCSS para el diseño web, al no tener experiéncia tube que hacer varias horas de formación para poder empezar con el proyecto.
- CORS y PreFlight del Backend. Rechazaban todas las peticiones.


### With ❤️ from Toni Ramon
