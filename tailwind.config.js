module.exports = {
  theme: {
    extend: {
      colors: {
        grey: {
          '300': '#D1D1D1'
        },
        black: {
          '300': '#484848'
        },
        main: {
          '500': '#FF5A5F'
        },
        secondary: {
          '500': '#FF5890'
        }
      }
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/ui'),
  ],
}