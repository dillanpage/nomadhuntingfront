import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)
axios.defaults.baseURL = process.env.VUE_APP_BACKEND_URL

export const store = new Vuex.Store({
    state: {
        token: localStorage.getItem('access_token') || null,
        userData: JSON.parse(localStorage.getItem('user_data')) || null
    },
    getters: {
        loggedIn(state) {
            return state.token !== null
        },
        userData(state) {
            return state.userData
        },
        token(state) {
            return state.token
        },
        isAdmin(state) {
            return state.userData.is_admin ? true : false
        },
    },
    mutations: {
        retrieveToken(state, token, userData) {
            state.token = token
            state.userData = userData
        },
        destroyToken(state) {
            state.token = null
            state.userData = null
        }
    },
    actions: {
        register(context, data) {
            return new Promise((resolve, reject) => {
                axios.post('/auth/signup', {
                    name: data.name,
                    email: data.email,
                    password: data.password,
                    password_confirmation: data.password
                })
                .then(response => {
                    resolve(response)
                })
                .catch(error => {
                    reject(error)
                })
            })
        },
        destroyToken(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token

                return new Promise((resolve, reject) => {
                    axios.get('/auth/logout')
                        .then(response => {
                            localStorage.removeItem('access_token')
                            localStorage.removeItem('user_data')
                            context.commit('destroyToken')
                            resolve(response)
                        })
                        .catch(error => {
                            localStorage.removeItem('access_token')
                            localStorage.removeItem('user_data')
                            context.commit('destroyToken')
                            reject(error)
                        })
                })
            }
        },
        retrieveToken(context, credentials) {
            return new Promise((resolve, reject) => {
                axios.post('/auth/login', {
                    email: credentials.email,
                    password: credentials.password,
                    remember_me: true
                })
                    .then(response => {
                        const token = response.data.access_token
                        const userData = JSON.stringify(response.data.user_data)
                        localStorage.setItem('access_token', token)
                        localStorage.setItem('user_data', userData)
                        context.commit('retrieveToken', token, userData)
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
        userLikePoi(context, poiId) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.post('/user/pois', {
                        id: poiId
                    })
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        userUnlikePoi(context, id) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.delete(`/user/pois/${id}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        deleteTravel(context, id) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.delete(`/user/travels/${id}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getLikedPois(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/user/pois')
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        addTravel(context, data) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.post('/user/travels', {
                        dateFrom: data.dateFrom,
                        dateTo: data.dateTo,
                        place: data.place,
                        pois: data.pois
                    })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        getTravels(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/user/travels')
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getTravel(context, travelId) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/user/travels/' + travelId)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        editTravel(context, travel) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.put('/user/travels/' + travel.travelId, {
                            pois: travel.pois
                        })
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getVisitedPois(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/user/travels/visited-pois')
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getMostLikedPois(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/admin/pois/liked')
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        addReview(context, data) {
            let formData = new FormData();
            formData.append('image', data.image);
            formData.append('description', data.description);
            formData.append('rating', data.rating);
            formData.append('poi_id', data.poi_id);
            formData.append('place_id', data.place_id);
            
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.post('/user/reviews', formData)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        getUserReviews(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/user/reviews')
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        deleteReview(context, id) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.delete(`/user/reviews/${id}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getPendingReviews(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/admin/reviews')
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        approveReview(context, id) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get(`/admin/reviews/approve/${id}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        denyReview(context, id) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get(`/admin/reviews/deny/${id}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getPrivateProfile(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/user/profile')
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        editUserProfile(context, data) {
            let formData = new FormData();
            formData.append('image', data.image);
            formData.append('user', JSON.stringify(data.user));

            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.post('/user/profile', formData)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        newSuggestion(context, suggestion) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.post('/user/suggestion', {
                        title: suggestion.title,
                        description: suggestion.description,
                        type: suggestion.type
                    })
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        getUserSuggestions(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/user/suggestions')
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        acceptSuggestion(context, suggestion) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.put('/user/suggestions/accept/' + suggestion.id, suggestion)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        denySuggestion(context, suggestion) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.put('/user/suggestions/deny/' + suggestion.id, suggestion)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        addPlace(context, data) {
            let formData = new FormData();
            formData.append('image', data.image);
            formData.append('place', JSON.stringify(data.place));
            
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.post('/admin/place', formData)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        deletePlace(context, place) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.delete(`/admin/places/${place.id}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getPlace(context, id) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get(`/admin/places/${id}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        editPlace(context, data) {
            let formData = new FormData();
            formData.append('image', data.image);
            formData.append('place', JSON.stringify(data.place));

            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.post('/admin/places/' + data.place.id, formData)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        getCategories(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/categories')
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        getDetailTypes(context) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get('/pois/details/types')
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        addPoi(context, data) {
            let formData = new FormData();
            formData.append('image', data.image);
            formData.append('poi', JSON.stringify(data.poi));
            
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.post('/admin/poi', formData)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        deletePoi(context, poi) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.delete(`/admin/pois/${poi.id}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getPoi(context, id) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get(`/admin/pois/${id}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getPoiCategories(context, id) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get(`/admin/pois/categories/${id}`)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        editPoi(context, data) {
            let formData = new FormData();
            formData.append('image', data.image);
            formData.append('poi', JSON.stringify(data.poi));

            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.post('/admin/pois/' + data.poi.id, formData)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
                })
            }
        },
        findUsersByPlace(context, place) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get(`/user/users/places/${place}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        findUsersByUsername(context, username) {
            if (context.getters.loggedIn) {
                // Set the header
                axios.defaults.headers.common['Authorization'] = 'Bearer ' + context.state.token
                return new Promise((resolve, reject) => {
                    axios.get(`/user/users/username/${username}`)
                        .then(response => {
                            resolve(response)
                        })
                        .catch(error => {
                            reject(error)
                        })
                })
            }
        },
        getPublicProfile(context, username) {
            return new Promise((resolve, reject) => {
                axios.get(`/users/${username}`)
                    .then(response => {
                        resolve(response)
                    })
                    .catch(error => {
                        reject(error)
                    })
            })
        },
    }
})