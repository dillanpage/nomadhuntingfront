
import Vue from 'vue'
import App from './App.vue'

// Event bus 
export const serverBus = new Vue();

import './assets/css/tailwind.css'
import './assets/css/style.css'

const moment = require('moment')
require('moment/locale/es')

require('vue2-animate/dist/vue2-animate.min.css')

Vue.config.productionTip = false

// Use Router
import VueRouter from 'vue-router'
Vue.use(VueRouter)

// Use CKEDITOR
import CKEditor from 'ckeditor4-vue';
Vue.use( CKEditor );

Vue.use(require('vue-moment'), {
  moment
})

import VueAxios from './plugins/axios'
Vue.use(VueAxios)

// Vuex management
import {store} from './store/store'

// Add Font Awesoem
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

// Add vue-functional-calendar
import FunctionalCalendar from 'vue-functional-calendar';
Vue.use(FunctionalCalendar, {
  dayNames: ['Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa', 'Do'],
  monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre']
});

// Import vuelidate
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)

// Import Vue2-leaflet
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';

Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);

// eslint-disable-next-line  
delete L.Icon.Default.prototype._getIconUrl  
// eslint-disable-next-line  
L.Icon.Default.mergeOptions({  
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),  
  iconUrl: require('leaflet/dist/images/marker-icon.png'),  
  shadowUrl: require('leaflet/dist/images/marker-shadow.png')  
})


export const router = new VueRouter({
  base: "/",
  mode: "history",
  routes: [
    { path: "/visited-pois", name:"visited-pois", component: () => import("./components/VisitedPoisComponent.vue")},
    { path: "/poi/**", name:"poi", component: () => import("./components/PoiComponent.vue")},
    { path: "/place/**", name:"place", component: () => import("./components/PlaceComponent.vue")},
    // Public user profile
    { path: "/@:username", name:"public.user", component: () => import("./components/user/PublicUserProfile.vue")},
    { path: "/login", name:"login", component: () => import("./components/auth/LoginComponent.vue"), 
        meta: {
          requiresVisitor: true
        }
    },
    { path: "/register", name:"register", component: () => import("./components/auth/RegisterComponent.vue"), 
      meta: {
        requiresVisitor: true
      }
    },
    { path: "/logout", name:"logout", component: () => import("./components/auth/LogoutComponent.vue")},
    { path: "/dashboard", name:"dashboard", component: () => import("./components/auth/DashboardComponent.vue"),
        meta: {
          requiresAuth: true
        },
        children: [
          // Dashboard Index will be rendered inside dashboard <router-view>
          { path: '', name: 'dashboard.index', component: () => import("./components/auth/user/UserHome.vue") },
          { path: 'likes', name: 'dashboard.likes', component: () => import("./components/auth/user/UserLikes.vue") },
          { path: 'new-travel', name: 'dashboard.newTravel', component: () => import("./components/auth/user/UserNewTravel.vue") },
          { path: 'visited-pois', name: 'dashboard.visitedPois', component: () => import("./components/auth/user/UserVisitedPois.vue") },
          { path: 'new-review/:poi_url', name: 'dashboard.newReview', component: () => import("./components/auth/user/UserNewReview.vue") },
          { path: "reviews", name:"dashboard.reviews", component: () => import("./components/auth/user/UserReviews.vue")},
          { path: 'travels', name: 'dashboard.travels', component: () => import("./components/auth/user/UserTravels.vue") },
          { path: "travels/**", name:"dashboard.travels.details", component: () => import("./components/auth/user/UserTravelDetails.vue")},
          { path: "profile", name:"dashboard.profile", component: () => import("./components/auth/user/UserProfile.vue")},
          { path: "suggestions", name:"dashboard.suggestions", component: () => import("./components/auth/user/UserSuggestions.vue")},
          { path: "find-users", name:"dashboard.findUsers", component: () => import("./components/auth/user/FindUsers.vue")},
        ]
    },
    { path: "/admin", name:"admin", component: () => import("./components/auth/AdminComponent.vue"),
        meta: {
          requiresAdmin: true
        },
        children: [
          // Dashboard Index will be rendered inside dashboard <router-view>
          { path: '', name: 'admin.index', component: () => import("./components/auth/admin/AdminHome.vue") },
          { path: 'reviews', name: 'admin.reviews', component: () => import("./components/auth/admin/AdminReviews.vue") },
          { path: 'liked-pois', name: 'admin.likedPois', component: () => import("./components/auth/admin/AdminLikedPois.vue") },
          { path: 'suggestions', name: 'admin.suggestions', component: () => import("./components/auth/admin/AdminSuggestions.vue") },
          
          // Place Routes
          { path: 'add-place', name: 'admin.addPlace', component: () => import("./components/auth/admin/AdminAddPlace.vue") },
          { path: 'manage-place', name: 'admin.managePlaces', component: () => import("./components/auth/admin/AdminManagePlaces.vue") },
          { path: 'edit-place/:place_id', name: 'admin.editPlace', component: () => import("./components/auth/admin/AdminEditPlace.vue") },

          // Poi Routes
          { path: 'add-poi', name: 'admin.addPoi', component: () => import("./components/auth/admin/AdminAddPoi.vue") },
          { path: 'manage-pois', name: 'admin.managePois', component: () => import("./components/auth/admin/AdminManagePois.vue") },
          { path: 'edit-poi/:poi_id', name: 'admin.editPoi', component: () => import("./components/auth/admin/AdminEditPoi.vue") },
        ]
    },

    { path: "/**", name:"home", component: () => import("./components/MainComponent.vue")},
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.loggedIn) {
      next({
        name: 'login',
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresAdmin)) {
    if (!store.getters.loggedIn) {
      next({
        name: 'login',
      })
    } else if (!store.getters.isAdmin) {
      next({
        name: 'dashboard.index',
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.requiresVisitor)) {
    if (store.getters.loggedIn) {
      next({
        name: 'dashboard.index',
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

new Vue({
  el: '#app',
  store: store,
  router, 
  render: h => h(App)
})



